

class Constants :

    class Menu :
        MENU_X = 50
        MENU_Y = 50
        MENU_MARGIN_X = 5
        MENU_MARGIN_Y = 5
        MENU_ITEM_PADDING = 5
        MENU_ITEM_HEIGHT = 40
        MENU_ITEM_WIDTH = 150
        MENU_LIST_BOX_ITEM_PADDING = 2

    class Colors :
        WHITE = (255 , 255 , 255)
        BLACK = (0 ,0 , 0)
        MENU_ITEM_BORDER = (160 , 160 , 160)
        MENU_SCROLL_BAR = (100 , 100 , 100)
        MENU_BUTTON = (200 , 200 , 200)
        TEXT = (20 , 20 , 20)
        FOCUS_BOX = (230 , 50 , 50)

    class Window :
        WIN_WIDTH = 1000
        WIN_HEIGHT = 700

    class Objects :
        pass

    class Math :
        GRAVITY = 9.81
        ro = None
        FRICTION = 0.5

    class Run :
        FRAMES = 60
        DT = 1/FRAMES
